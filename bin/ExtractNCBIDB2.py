#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    A copy of the GNU General Public License is available at
#    http://www.gnu.org/licenses/gpl-3.0.html

"""Extract the annotation in case of blast on imomi database."""

import os
import sys
import argparse
import csv
from pathlib import Path
from typing import Dict, List

__author__ = "Amine Ghozlane"
__copyright__ = "Copyright 2023, Institut Pasteur"
__license__ = "GPL"
__version__ = "1.1.0"
__maintainer__ = "Amine Ghozlane"
__email__ = "amine.ghozlane@pasteur.fr"
__status__ = "Developpement"


def isfile(path: str) -> Path:  # pragma: no cover
    """Check if path is an existing file.

    :param path: (str) Path to the file

    :raises ArgumentTypeError: If file does not exist

    :return: (Path) Path object of the input file
    """
    myfile = Path(path)
    if not myfile.is_file():
        if myfile.is_dir():
            msg = f"{myfile.name} is a directory."
        else:
            msg = f"{myfile.name} does not exist."
        raise argparse.ArgumentTypeError(msg)
    return myfile


def isdir(path: str) -> Path:  # pragma: no cover
    """Check if path can be valid directory.

    :param path: Path to the directory

    :raises ArgumentTypeError: If directory does not exist

    :return: (str) Path object of the directory
    """
    mydir = Path(path)
    # if not mydir.is_dir():
    if mydir.is_file():
        msg = f"{mydir.name} is a file."
        raise argparse.ArgumentTypeError(msg)
        # else:
        #     msg = f"{mydir.name} does not exist."
    return mydir


# ===================
# parameters
# ===================
def get_arguments():
    """Extract program options"""
    parser = argparse.ArgumentParser(
        description=__doc__, usage="{0} -h [options] [arg]".format(sys.argv[0])
    )
    parser.add_argument(
        "-f",
        "--BlastResultFile",
        dest="blast_result_file",
        type=isfile,
        required=True,
        help="Input blast result file, in m8 mode.",
    )
    parser.add_argument(
        "-g",
        "--gi_taxid_taxonomy",
        dest="taxonomy_file",
        type=isfile,
        required=True,
        help="gi_taxid_taxonomy file for gi to taxid taxonomy " "correspondancy",
    )
    parser.add_argument(
        "-nb",
        dest="nbest",
        type=int,
        default=0,
        help="Number of best selected (default:0 - "
        "based on the number of based aligned)",
    )
    parser.add_argument(
        "-fc",
        dest="filter_coverage",
        type=float,
        default=0,
        help="Filter the coverage (default >= 0 percent).",
    )
    parser.add_argument(
        "-fi",
        dest="filter_identity",
        action="store_true",
        default=False,
        help="Remove filtering on the identity (default False, "
        "superkingdom >= 0.0, phylum >= 65, class >= 75, "
        "genus >= 85, specie >=95 percent ).",
    )
    parser.add_argument(
        "-id", dest="identity", type=str, default=None, help="Sample identity"
    )
    parser.add_argument(
        "-o", "--output_file", dest="output_file", type=Path, help="Output file"
    )
    parser.add_argument(
        "-r",
        dest="results",
        type=isdir,
        default=os.curdir + os.sep,
        help="Path to result " "directory.",
    )
    return parser.parse_args()


def parse_acc_to_taxid_taxonomy_file(taxonomy_file: Path) -> Dict[str, str]:
    """Get association between accession number with taxid"""
    acc_taxid_taxonomy_dict: Dict[str, str] = {}
    try:
        with taxonomy_file.open("rt") as taxonomy:
            taxonomy_reader = csv.reader(taxonomy, delimiter="\t")
            # pass header
            next(taxonomy_reader)
            for line in taxonomy_reader:
                # print(line[0])
                acc_taxid_taxonomy_dict[line[0]] = line[2]
    except IOError:
        sys.exit("Error cannot open {0}".format(taxonomy_file))
    return acc_taxid_taxonomy_dict


def extract_annotation(
    blast_result_file: Path, acc_taxid_taxonomy_dict: Dict[str, str]
) -> Dict[str, List]:
    """Extract blast annotation"""
    blast_dict: Dict[str, List] = {}
    try:
        # Remove query dict
        with open(blast_result_file, "rt") as blast_result:
            blast_reader = csv.reader(blast_result, delimiter="\t")
            for line in blast_reader:
                if "|" in line[1]:
                    acc = line[1].split("|")[2].split(".")[0]
                else:
                    acc = line[1].split(".")[0]
                # print(acc)
                if acc in acc_taxid_taxonomy_dict:
                    annotation = acc_taxid_taxonomy_dict[acc]
                else:
                    annotation = None
                # id identity coverage
                if line[0] in blast_dict:
                    blast_dict[line[0]] += [
                        [
                            acc,
                            annotation,
                            float(line[10]),
                            float(line[11]),
                            float(line[12]),
                            float(line[13]),
                        ]
                    ]
                else:
                    blast_dict[line[0]] = [
                        [
                            acc,
                            annotation,
                            float(line[10]),
                            float(line[11]),
                            float(line[12]),
                            float(line[13]),
                        ]
                    ]
            assert len(blast_dict) > 0
    except IOError:
        sys.exit("Error cannot open {0}".format(blast_result_file))
    except AssertionError:
        sys.exit("Error nothing read from {0}".format(blast_result_file))
    return blast_dict


# filter_identity,
def write_annotation(
    blast_dict: Dict[str, List],
    nbest: int,
    filter_coverage: float,
    filter_identity: float,
    output_file: Path,
    results: Path,
    identity,
):
    """Write the result"""
    idname = ""
    if identity:
        idname = identity + "_"
    if not output_file:
        output_file = results / "ncbi_taxonomic_annotation.txt"
    try:
        with open(output_file, "wt") as output:
            output_writer = csv.writer(output, delimiter="\t")
            # output_writer.writerow(["ContigName", "GI", "superkingdom", "kingdom",
            #                        "phylum", "class", "order", "family",
            #                        "genus","species", "PourcID",
            #                        "Coverage", "evalue"])
            for key in blast_dict:
                # print(key)
                # if filter_coverage > 0:
                data = []
                for element in blast_dict[key]:
                    # print(element)
                    if element[3] >= filter_coverage:
                        data.append(element)
                # else:
                # data = blast_data[key]
                # print(data)
                # Sort depending on the identity + coverage
                data.sort(key=lambda x: x[2] + x[3], reverse=True)
                if nbest > 0:
                    short_set = data[0:nbest]
                else:
                    short_set = data
                # print(short_set)
                for hit in short_set:
                    # print(hit)
                    if hit[1]:
                        if hit[2] >= 95.0 or filter_identity:
                            hit[1] = hit[1].split(";")
                        elif hit[2] >= 85.0:
                            hit[1] = hit[1].split(";")[0:7] + ["NA"]
                        elif hit[2] >= 75.0:
                            hit[1] = hit[1].split(";")[0:4] + ["NA"] * 4
                        elif hit[2] >= 65.0:
                            hit[1] = hit[1].split(";")[0:3] + ["NA"] * 5
                        elif hit[1] > 0.0:
                            hit[1] = hit[1].split(";")[0:2] + ["NA"] * 6
                    else:
                        hit[1] = ["NA"] * 9
                for element in short_set:
                    # print(element)
                    output_writer.writerow(
                        [idname + key, element[0]] + element[1] + element[2:]
                    )
    except IOError:
        sys.exit("Error cannot open {0}".format(output_file))


def main():
    """Main program"""
    args = get_arguments()
    # Parse accession id to taxid
    acc_taxid_taxonomy_dict = parse_acc_to_taxid_taxonomy_file(args.taxonomy_file)
    # Parse blast result
    blast_dict = extract_annotation(args.blast_result_file, acc_taxid_taxonomy_dict)
    # Write annotation
    write_annotation(
        blast_dict,
        args.nbest,
        args.filter_coverage,
        args.filter_identity,
        args.output_file,
        args.results,
        args.identity,
    )


if __name__ == "__main__":
    main()
