#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    A copy of the GNU General Public License is available at
#    http://www.gnu.org/licenses/gpl-3.0.html
import os
import sys
import argparse
from pathlib import Path


def isfile(path: str) -> Path:  # pragma: no cover
    """Check if path is an existing file.

    :param path: (str) Path to the file

    :raises ArgumentTypeError: If file does not exist

    :return: (Path) Path object of the input file
    """
    myfile = Path(path)
    if not myfile.is_file():
        if myfile.is_dir():
            msg = f"{myfile.name} is a directory."
        else:
            msg = f"{myfile.name} does not exist."
        raise argparse.ArgumentTypeError(msg)
    return myfile


def isdir(path: str) -> Path:  # pragma: no cover
    """Check if path can be valid directory.

    :param path: Path to the directory

    :raises ArgumentTypeError: If directory does not exist

    :return: (str) Path object of the directory
    """
    mydir = Path(path)
    # if not mydir.is_dir():
    if mydir.is_file():
        msg = f"{mydir.name} is a file."
        raise argparse.ArgumentTypeError(msg)
        # else:
        #     msg = f"{mydir.name} does not exist."
    return mydir


def getArguments():
    """Retrieves the arguments of the program.
    Returns: An object that contains the arguments
    """
    # Parsing arguments
    parser = argparse.ArgumentParser(
        description=__doc__, usage="{0} -h".format(sys.argv[0])
    )
    parser.add_argument(
        "-i",
        dest="fastq_file",
        type=isfile,
        required=True,
        help="Path to the fastq file.",
    )
    parser.add_argument(
        "-o", dest="output_file", type=Path, default=None, help="Output file."
    )
    parser.add_argument(
        "-r",
        dest="results",
        type=isdir,
        default=Path(os.curdir),
        help="Path to result directory.",
    )
    args = parser.parse_args()
    return args


def convert_fastq_fasta(fastq_file: Path, output_file: Path) -> None:
    """Convert fastq file to fasta file"""
    if not output_file:
        output = sys.stdout
    else:
        output = output_file.open("wt")
    try:
        with output:
            with fastq_file.open("rt") as fastq:
                for line in fastq:
                    header = line[1:]
                    line = next(fastq)
                    print(">{0}{1}".format(header, line.replace("\n", "")), file=output)
                    next(fastq)
                    next(fastq)
    except IOError:
        sys.exit("Error cannot open {0}".format(fastq_file))


def main():
    """Main program"""
    args = getArguments()
    convert_fastq_fasta(args.fastq_file, args.output_file)


if __name__ == "__main__":
    main()
