#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    A copy of the GNU General Public License is available at
#    http://www.gnu.org/licenses/gpl-3.0.html
import os
import sys
import argparse
import csv


def isfile(path):
    """Check if path is an existing file.
    Arguments:
        path: Path to the file
    """
    if not os.path.isfile(path):
        if os.path.isdir(path):
            msg = "{0} is a directory".format(path)
        else:
            msg = "{0} does not exist.".format(path)
        raise argparse.ArgumentTypeError(msg)
    return path


def getArguments():
    """Retrieves the arguments of the program.
    Returns: An object that contains the arguments
    """
    # Parsing arguments
    parser = argparse.ArgumentParser(
        description=__doc__, usage="{0} -h".format(sys.argv[0])
    )
    parser.add_argument(
        "-i",
        dest="resume_list",
        type=isfile,
        required=True,
        nargs="+",
        help="Path to the fastq file.",
    )
    parser.add_argument(
        "-m",
        dest="map_list",
        type=isfile,
        required=True,
        nargs="+",
        help="Path to the fastq file.",
    )
    parser.add_argument(
        "-mg",
        dest="map_genome_list",
        type=isfile,
        required=True,
        nargs="+",
        help="Path to the fastq file.",
    )
    parser.add_argument(
        "-o", dest="output_file", type=str, default=None, help="Output file."
    )
    args = parser.parse_args()
    return args


def load_csv(csv_file, data_dict, sample):
    """ """
    try:
        with open(csv_file, "rt") as csv_dat:
            csv_reader = csv.reader(csv_dat, delimiter="\t")
            for line in csv_reader:
                if sample in data_dict:
                    data_dict[sample].update({line[0]: int(line[1])})
                else:
                    data_dict[sample] = {line[0]: int(line[1])}
    except IOError:
        sys.exit("Error cannot open {0}".format(csv_file))
    return data_dict


def load_bowtie_resume(resume_file, data_dict, sample, organism):
    """ """
    val = 0
    try:
        with open(resume_file, "rt") as resume:
            # Pass 3 line
            resume.next()
            resume.next()
            resume.next()
            val = int(resume.next().strip().split(" ")[0])
            val += int(resume.next().strip().split(" ")[0])
            data_dict[sample].update(
                {"Number of reads mapped against {0}".format(organism): val}
            )
    except IOError:
        sys.exit("Error cannot open {0}".format(resume_file))
    return data_dict


def write_result(data_dict, output_file):
    """ """
    key_list = [
        "Number of raw reads",
        "Number of reads mapped against human",
        "Number of reads mapped against mouse",
        "Number of reads mapped against phi",
        "Number of reads after filtering",
        "Number of reads after trimming",
        "Number of reads mapped against genome set",
        "Number of reads annotated with BLAST",
        "Number of reads annotated with DIAMOND",
    ]
    sample_list = data_dict.keys()
    try:
        with open(output_file, "wt") as output_dat:
            output_writer = csv.writer(output_dat, delimiter="\t")
            output_writer.writerow(["Sample"] + sample_list)
            for key in key_list:
                val = [key]
                for sample in sample_list:
                    val += [data_dict[sample][key]]
                output_writer.writerow(val)
    except IOError:
        sys.exit("Error cannot open {0}".format(output_file))


def main():
    """Main program"""
    organism = "unknown"
    data_dict = {}
    args = getArguments()
    sample = ""
    for resume in args.resume_list:
        sample = resume.replace("_resume_nb_reads.txt", "")
        sample = sample.replace("_trim_nb_reads.txt", "")
        sample = sample.replace("_tax.txt", "")
        sample = sample.replace("_taxdia.txt", "")
        data_dict = load_csv(resume, data_dict, sample)
    sample = ""
    for resume in args.map_list:
        if "_mapping_human.txt" in resume:
            sample = resume.replace("_mapping_human.txt", "")
            organism = "human"
        elif "_mapping_mouse.txt" in resume:
            sample = resume.replace("_mapping_mouse.txt", "")
            organism = "mouse"
        elif "_mapping_phi.txt" in resume:
            sample = sample.replace("_mapping_phi.txt", "")
            organism = "phi"
        else:
            print("something wrong with map file: {0}".format(resume), file=sys.stderr)
        data_dict = load_bowtie_resume(resume, data_dict, sample, organism)
    for resume in args.map_genome_list:
        sample = resume.replace(".txt", "")
        data_dict = load_bowtie_resume(resume, data_dict, sample, "genome set")
    # print(data_dict)
    write_result(data_dict, args.output_file)


if __name__ == "__main__":
    main()
