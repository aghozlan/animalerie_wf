#!/usr/bin/env nextflow

workflow.onComplete = {
    // any workflow property can be used here
    println "Pipeline complete"
    println "Command line: $workflow.commandLine"
}


workflow.onError = {
    println "Oops .. something went wrong"
}

// General parameters
params.in="${baseDir}/test/"
params.cpus = 6
params.mail = "amine.ghozlane@pasteur.fr"
params.out = "${HOME}/"
params.kronaout = "${params.out}/krona.html"
params.resumeout = "${params.out}/resume_table.tsv"
params.annotationout = "${params.out}/annotation_table.tsv"
params.countout = "${params.out}/count_table.tsv"
// Annotation parameters
params.identity = 50
params.dia_identity = 40
params.coverage = 50
params.dia_coverage = 40
params.evalue = 1E-3
params.hit = 1
params.wordsize = 28
params.minlength = 35
params.memory_mbma = 30000
// Databases
params.alienseq = "$baseDir/databases/alienTrimmerPF8contaminants.fasta"
params.genomes = "$baseDir/databases/genomes_resume.fasta"
params.human = "/local/databases/index/bowtie/2.1.0/hg38.fa"
params.mouse = "/local/databases/index/bowtie/2.1.0/mm10.fa"
params.phi = "/local/databases/index/bowtie/2.1.0/phiX.fa"
params.nt = "/local/databases/fasta/nt"
params.taxadb = "/local/databases/rel/taxadb/current/db/taxadb_full.sqlite"
params.nrdb = "/local/databases/rel/nrprot/current/diamond/2.0/nr.dmnd"
myDir = file(params.out)
myDir.mkdirs()


params.help=false

def usage() {
    println("animalerie-wf.nf --in <input_tsv> --out <output_dir>")
    println("--in Directory containing fastq files (Single end only, in format .fastq or .fastq.gz, default ${params.in}).")
    println("--out Output directory (default ${params.out}). ")
    println("--kronaout Output krona file (default ${params.out}krona.html)")
    println("--resumeout Output resume file (default ${params.out}resume_table.tsv)")
    println("--annotationout Output annotation file (default ${params.out}annotation_table.tsv)")
    println("--countout Output count file (default ${params.out}count_table.tsv)")
    println("--cpus Number of cpus for process (default ${params.cpus})")
    println("-w Temporary output (usually /pasteur/scratch/animalerie-wf)")
    println("--identity Read minimum identity with blast in percent (Default ${params.identity})")
    println("--coverage Read minimum coverage with blast in percent (Default ${params.coverage})")
    println("--dia_identity Read minimum identity with diamond in percent (Default ${params.dia_identity})")
    println("--dia_coverage Read minimum coverage with diamond in percent (Default ${params.dia_coverage})")
    println("--evalue E-value threshold (Default ${params.evalue})")
}

if(params.help){
    usage()
    exit(1)
}

process fastqfiltering {
    memory "4G"
    cpus params.cpus
    //module "bowtie2/2.5.1"

    afterScript """
        nb_raw=\$(echo \$((`wc -l < ${reads}` / 4)));
        echo -e "Number of raw reads\t\$nb_raw" > ${sample_id}_resume_nb_reads.txt;
        nb_filt=\$(echo \$((`wc -l < ${sample_id}_notmapped.fastq` / 4)));
        echo -e "Number of reads after filtering\t\$nb_filt" >> ${sample_id}_resume_nb_reads.txt
        """

    input:
    tuple val(sample_id), path(reads)

    // output:
    // tuple sample_id, path "*_notmapped.fastq"
    // path "*_mapping_*.txt" //into mapChannel  mode flatten
    // path "*_resume_*.txt"

    """
    bowtie2 -p ${params.cpus}  --sensitive-local -x ${params.human}\
        -U ${reads} -S /dev/null \
        --un ${sample_id}_notmapped_human.fastq > ${sample_id}_mapping_human.txt 2>&1
    bowtie2 -p ${params.cpus}  --sensitive-local -x ${params.mouse}\
        -U ${sample_id}_notmapped_human.fastq -S /dev/null \
        --un ${sample_id}_notmapped_mouse.fastq > ${sample_id}_mapping_mouse.txt 2>&1
    bowtie2 -p ${params.cpus}  --sensitive-local -x ${params.phi}\
        -U ${sample_id}_notmapped_mouse.fastq -S /dev/null \
        --un ${sample_id}_notmapped.fastq > ${sample_id}_mapping_phi.txt 2>&1
    """
}

// process trimming {
//     memory "4G"
//     module "AlienTrimmer/2.0"

//     afterScript """
//         nb_trim=\$(echo \$((`wc -l < !{sample_id}.fastq` / 4)));
//         echo -e "Number of reads after trimming\t\$nb_trim" > !{sample_id}_trim_nb_reads.txt
//         """

//     input:
//     set sample_id, file(reads) from filteringChannel

//     output:
//     file("*.fastq") into trimmingChannel
//     file("*.txt") into resumetrimChannel

//     """
//     AlienTrimmer -i !{reads} -o !{sample_id}.fastq  -c !{params.alienseq} \
//         -l !{params.minlength}
//     """
// }


// process mbma {
//     memory "4G"

//     input:
//     file(reads) from trimmingChannel.toList()

//     output:
//     file("count_matrix.tsv") into countChannel
//     file("count_matrix.tsv") into countbisChannel
//     file("not_aligned/*.temp") into naChannel mode flatten
//     file("abundance/out/*txt") into mapgenomeChannel mode flatten

//     //TODO: replace by meteor
//     shell:
//     """
//     #!/bin/bash
//     mbma.py mapping -se ${reads.join(" ")} -o abundance --best -m SE -t ${params.cpus} \
//            -db ${params.genomes} -e !{params.mail} -q ${params.queue} \
//            -p ${params.partition} --bowtie2 -mem ${params.memory_mbma}
//     mkdir not_aligned/
//     for infile in \$(ls abundance/unmapped/*)
//     do
//         nn=`basename \$infile`
//         mv \$infile not_aligned/\$nn.temp
//     done
//     mv abundance/comptage/count_matrix.tsv ./count_matrix.tsv
//     """
// }


// process mbma_krona {
//     memory "1G"
//     shell '/bin/bash', '-euo', 'pipefail'

//     input:
//     file(counts) from countChannel

//     output:
//     file("*.txt") into mbmataxChannel mode flatten

//     """
//     i=1
//     for samp in \$(head -n1 !{counts} | cut -f 3- -d \$'\t'); do
//         tail -n +2 !{counts} | cut -f \$((2 + \$i)),1 -d \$'\t' | awk -F \$'\t' '{print \$2, \$1}' OFS=\$'\t' > \${samp}_mbma_count.txt
//         i=\$((\$i + 1))
//     done
//     """
// }



// process fastqtofasta {
//     memory "2G"
//     module "Python/3.11.5"

//     input:
//     set sample_id, file(reads) from notalignedChannel

//     output:
//     set sample_id, file("*.fasta") into notalignedfastaChannel

//     """
//     fastq2fasta.py -i ${reads} -o ${sample_id}.fasta
//     """
// }

// process blast {
//     cpus params.cpus
//     memory "30G"
//     module "blast+/2.14.1"

//     input:
//     set sample_id, file(fasta) from notalignedfastaChannel

//     output:
//     set sample_id, file(fasta), file("*_nt.txt") into blastChannel mode flatten

//     """
//     blastn -query ${fasta} -out ${sample_id}_nt.txt -outfmt \
//            "6 qseqid sseqid qlen length mismatch gapopen qstart qend sstart send pident qcovs evalue bitscore" \
//            -db ${params.nt}  -max_target_seqs ${params.hit} \
//            -evalue ${params.evalue} -num_threads ${params.cpus} \
//            -perc_identity ${params.identity} -qcov_hsp_perc ${params.coverage} \
//            -word_size ${params.wordsize}
//     """
// }

// process taxonomy {
//     module "Python/3.11.5:taxadb/0.12.0"
//     memory "10G"
//     shell '/bin/bash', '-euo', 'pipefail'

//     input:
//     set sample_id, file(fasta), file(nt) from blastChannel

//     output:
//     set sample_id, file(fasta), file("*_not_annotated.fasta"), file("*_annotation.txt") into notAnnotatedChannel
//     file("*_tax.txt") into resumetaxChannel

//     """
//     #!/bin/bash
//     tax_count=\$(wc -l !{nt} |cut -f 1 -d " ")
//     if [ "\$tax_count" -gt "0" ]
//     then
//         # Annot ncbi
//         get_taxonomy3.py -i !{nt} -d !{params.taxadb} \
//             -o !{sample_id}_taxonomy.txt
//         ExtractNCBIDB2.py -f !{nt} -g !{sample_id}_taxonomy.txt -nb 1 \
//             -o !{sample_id}_annotation.txt
//         # Get sequence not annotated
//         if [ -f "!{sample_id}_annotation.txt" ]
//         then
//             extract_fasta.py -q !{sample_id}_annotation.txt \
//                  -t !{fasta} -n -o !{sample_id}_not_annotated.fasta
//         else
//             touch !{sample_id}_annotation.txt !{sample_id}_not_annotated.fasta
//         fi

//     else
//         cat !{fasta} > !{sample_id}_not_annotated.fasta
//         touch !{sample_id}_annotation.txt !{sample_id}_taxonomy.txt
//     fi

//     nb_annot=`wc -l < !{sample_id}_annotation.txt`
//     echo -e "Number of reads annotated with BLAST\t\$nb_annot" > !{sample_id}_tax.txt
//     """
// }

// process diamond {
//     cpus params.cpus
//     memory "15G"
//     module "diamond/2.0.15"

//     input:
//     set sample_id, file(fasta), file(notannotatedfasta), file(taxblast) from notAnnotatedChannel

//     output:
//     set sample_id, file(fasta), file("*_dia.txt") , file(taxblast) optionnal: true into diamondChannel

//     when notannotatedfasta.countFasta > 0

//     """"
//     diamond blastx -d ${params.nrdb} -q ${notannotatedfasta} -o ${sample_id}_dia.txt \
//         --outfmt 6 qseqid sseqid qlen length mismatch gapopen qstart qend sstart send pident qcovhsp evalue bitscore \
//         -e ${params.evalue} -k 1 -p ${params.cpus} --id ${params.dia_identity} \
//         --query-cover ${params.dia_coverage}
//     """


// }

// process annotation_for_krona {
//     module "Python/3.11.5:taxadb/0.12.0"
//     memory "4G"

//     input:
//     set sample_id, file(fasta), file(dia), file(taxblast) from diamondChannel
//     val tax_count = dia.countLines

//     output:
//     file("*_kronablast.txt") into taxChannel
//     file("*_taxdia.txt") into resumetaxdiaChannel
//     file("*_final_annotation.txt") into finalannotationChannel

//     afterScript:
//     """
//     # Interest column for krona
//     cut -s -f 3-10 !{sample_id}_final_annotation.txt > !{sample_id}_annotation_interest.txt
//     # count number of elements in annotated compared to the number of sequence
//     # to annot
//     count_reads=\$(grep "^>" -c !{fasta})
//     # Create Krona annotation
//     while read line; do echo -e "1\t\$line"; done < !{sample_id}_annotation_interest.txt > !{sample_id}_krona.txt
//     cat !{sample_id}_krona.txt > not-annotated-!{sample_id}_kronablast.txt
//     annot=\$(wc -l < !{sample_id}_krona.txt)
//     # Count not annoted elements
//     if [ "\$count_reads" -gt "\$annot" ]; then
//         val=\$(( count_reads - annot))
//         echo -e "\$val\tNA\tNA\tNA\tNA\tNA\tNA\tNA" >> not-annotated-!{sample_id}_kronablast.txt
//     fi

//     nb_annot=\$(wc -l < !{sample_id}_dia_annotation.txt)
//     echo -e "Number of reads annotated with DIAMOND\t\$nb_annot" > !{sample_id}_taxdia.txt
//     """

//     if(tax_count > 0){
//         """
//         // Annot ncbi
//         get_taxonomy3.py -i !{dia} \
//                 -d !{params.taxadb} \
//                 -o !{sample_id}_dia_taxonomy.txt
//         ExtractNCBIDB2.py -f !{dia} \
//                 -g !{sample_id}_dia_taxonomy.txt -nb 1 -o !{sample_id}_dia_annotation.txt
//         cat !{taxblast} !{sample_id}_dia_annotation.txt > !{sample_id}_final_annotation.txt
//         """
//     }
//     else{
//         """
//         cat !{taxblast} > !{sample_id}_final_annotation.txt
//         touch !{sample_id}_dia_annotation.txt
//         """
//     }
// }


// process krona {
//     memory "4G"
//     module "KronaTools/2.7.1"
//     beforeScript """
//         files=""
//         for i in `ls *.txt`
//         do
//             name=\$(echo \$i | cut -f 1 -d "_" )
//             nb_line=\$(wc -l < \$i)
//             if [ "\$nb_line" -gt "0" ]
//             then
//                 files="\${files} \$i,\${name} "
//             fi
//         done
//         """

//     input:
//     file(kr) from taxChannel.toList()
//     file(mbma_kr) from mbmataxChannel.toList()

//     output:
//     file("res.html") into kronaChannel

//     """
//     ktImportText \${files} -o res.html
//     """
// }


// process resume {
//     module "Python/3.11.5"

//     input:
//     file(resmap) from resumemapChannel.toList()
//     file(map) from mapChannel.toList()
//     file(restrim) from resumetrimChannel.toList()
//     file(restax) from resumetaxChannel.toList()
//     file(restaxdia) from resumetaxdiaChannel.toList()
//     file(resmapgenome) from mapgenomeChannel.toList()

//     output:
//     file("resume.tsv") into resumeout

//     """
//     resume.py -i ${resmap} ${restrim} ${restax} ${restaxdia} -m ${map} -mg ${resmapgenome} -o resume.tsv
//     """
// }


workflow {
    fastqChannel = Channel.fromPath("${params.in}")
                    .ifEmpty { exit 1, "Cannot find read file: ${params.in}" }
                     .splitCsv(sep: "\t")
                     .groupTuple()
                     .map{it -> [it[0], it[1][0]] }
                     .subscribe onNext: { println it }, onComplete: { println 'Done' }
    fastqfiltering(fastqChannel)
    // notalignedChannel = naChannel.map {
    //                             tmp = it.baseName
    //                             tuple(tmp.split('\\.')[0], it)
    //                         }
    // countbisChannel.subscribe { it.copyTo("${params.countout}") }
    // kronaChannel.subscribe { it.copyTo("${params.kronaout}") }
    // finalannotationChannel.collectFile(name: 'combined.tsv').subscribe { it.copyTo("${params.annotationout}") }
    // resumeout.subscribe { it.copyTo("${params.resumeout}") }
    // println "Project : $workflow.projectDir"
    // println "Cmd line: $workflow.commandLine"

}
